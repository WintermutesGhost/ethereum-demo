pragma solidity ^0.4.18;
// Specify version of compiler to use

contract Voting {

  /* mapping field below is like associative array/hash.
     Key of mapping is candidate name stored as bytes32,
     value is unsigned int of vote count.
  */

  mapping (bytes32 => uint8) public votesReceived;

  /* Solidity connot pass array of strings, so array of 
     bytes32 to store list of candidates
  */

  bytes32[] public candidateList;

  /* Constructor to call once contract deployed. WHen 
     deployed, pass array of candidates.
  */

  function Voting(bytes32[] candidateNames) public {
    candidateList = candidateNames;
  }

  // Function to return total votes received by cand
  function totalVotesFor(bytes32 candidate) view public returns (uint8) {
    require(validCandidate(candidate));
    return votesReceived[candidate];
  }

  // Increment vote count for candidate (cast vote)
  function voteForCandidate(bytes32 candidate) public {
    require(validCandidate(candidate));
    votesReceived[candidate] += 1;
  }

  // Validate candidate is found
  function validCandidate(bytes32 candidate) view public returns (bool) {
    for(uint i = 0; i < candidateList.length; i++) {
      if (candidateList[i] == candidate) {
	return true;
      }
    }
    return false;
  }
}

